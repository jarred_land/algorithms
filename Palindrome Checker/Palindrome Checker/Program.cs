﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;


namespace Palindrome_Checker
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack palinstack = new Stack();
            Console.WriteLine("This program will check a string for palindromic activity");
            string input = Console.ReadLine();

            foreach (char c in input.ToLower())
            {
                palinstack.Push(c);
            }

            string popresult = string.Empty;

            while (palinstack.Count > 0)
            {
                popresult += palinstack .Pop();
            }

            if (input.ToLower() == popresult.ToLower())
            {
                Console.WriteLine("This string is a palindrome");
            }
            else
            {
                Console.WriteLine("This string is not a palindrome");
            }

            Console.ReadLine();
        }
    }
}
