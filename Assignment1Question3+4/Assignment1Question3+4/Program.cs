﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Assignment1Question3_4
{
    public class Evaluate
    {
        private static Dictionary<string, int[]> dictionary = new Dictionary<string, int[]>();

        private static int GetPrecedence(string s)
        {
            return dictionary[s][0];
        }​
        private static int GetAssociation(string s)
        {
            return dictionary[s][1];
        }​
        public static Queue<string> InfixToPostFix(string expression)
        {
            Queue<string> queue = new Queue<string>();
            Stack<string> stack = new Stack<string>();

            dictionary.Add("*", new int[] { 4, 0 });
            dictionary.Add("/", new int[] { 4, 0 });
            dictionary.Add("+", new int[] { 3, 0 });
            dictionary.Add("-", new int[] { 3, 0 });
​
            expression = expression.Replace(" ", "");
            string pattern = @"(?<=[-+*/(),^<>=&])(?=.)|(?<=.)(?=[-+*/(),^<>=&])";
​
            Regex regExPattern = new Regex(pattern);
​
            regExPattern.Split(expression).Where(s => !String.IsNullOrEmpty(s.Trim())).ToList().ForEach(s =>
            {
                if (dictionary.ContainsKey(s))
                {
                    while (stack.Count > 0 && stack.Peek() != "(")
                    {
                        if ((GetAssociation(s) == 0 && GetPrecedence(s) <= GetPrecedence(stack.Peek())) ||
                            (GetAssociation(s) == 1 && GetPrecedence(s) < GetPrecedence(stack.Peek()))
                          )
                            queue.Enqueue(stack.Pop());
                        else
                            break;
                    }
​
                    stack.Push(s);
                }

                else if (s == "(")
                {
                    stack.Push(s);
                }

                else if (s == ")")
                {
                    while (stack.Count != 0 && stack.Peek() != "(")
                        queue.Enqueue(stack.Pop());
​
                    stack.Pop();
                }
                else
                    queue.Enqueue(s);
            });

            while (stack.Count != 0)
                queue.Enqueue(stack.Pop());
​
            return queue;​
        }​
        public static double CalculateExpression(Queue<String> postfix)
        {
            Stack<double> stack = new Stack<double>();
​
            postfix.ToList<String>().ForEach(token =>
            {
                if (dictionary.ContainsKey(token))
                {
                    if (stack.Count > 1)
                    {
                        double rhs = stack.Pop();
                        double lhs = stack.Pop();
                        switch (token)
                        {
                            case "+":
                                stack.Push(lhs + rhs);
                                break;
                            case "-":
                                stack.Push(lhs - rhs);
                                break;
                            case "*":
                                stack.Push(lhs * rhs);
                                break;
                            case "/":
                                stack.Push(lhs / rhs);
                                break;
                        }
                    }
                }
                else
                    stack.Push(Convert.ToDouble(token));
            });
            return stack.Pop();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter an Infix expression: ");
            string exp = Console.ReadLine();​
            Console.Write("Postfix Expression: ");
            Queue<string> q = Evaluate.InfixToPostFix(exp);​

            foreach (string s in q) {
                Console.Write(s);​
                Console.WriteLine("\nThe expression evaluates to: {0}", Evaluate.CalculateExpression(q));
                Console.Read();
            }
        }
    }
}