﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Assignment1Queues
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue stuff = new Queue(); 
            object[] itemsarray = new object[2]; 
            int menuChoice = 0;
            while (menuChoice != 6)
            {
                Console.WriteLine("MENU\n"
                    + "\n1. Add something"
                    + "\n2. Delete stuff"
                    + "\n3. Find a thing"
                    + "\n4. Copy object into an array"
                    + "\n5. Display all the things in the queue"
                    + "\n6. Exit");

                Console.Write("\nSelect a number from the menu: ");
                menuChoice = int.Parse(Console.ReadLine());
                switch (menuChoice)
                {
                    case 1: 
                        Console.Clear();
                        MyQueue.Enqueue(stuff);
                        break;
                    case 2:
                        Console.Clear();
                        MyQueue.Dequeue(stuff);
                        break;
                    case 3:
                        Console.Clear();
                        MyQueue.Contains(stuff);
                        break;
                    case 4:
                        Console.Clear();
                        MyQueue.ToArray(stuff, itemsarray);
                        break;
                    case 5:
                        Console.Clear();
                        MyQueue.PrintQueue(stuff);
                        break;
                    case 6:
                        Console.Clear();
                        Console.Write("Press enter to exit the program ");
                        break;
                    default: 
                        Console.Clear();
                        Console.Write("Invalid selection ");
                        break;
                }
                Console.ReadLine(); 
            }
        }
    }

    class MyQueue 
    {
        public static void PrintQueue(Queue stuff)
        {
            foreach (string s in stuff) { 
                Console.WriteLine(s + "");
            }
            Console.Write("\nPress enter to return to the menu screen: ");
        }
        public static void Enqueue(Queue stuff)
        {
            Console.Write("Enter a thing: ");
            string input = Console.ReadLine();
            stuff.Enqueue(input); 
​
            while (input.Contains(""))
            {
                Console.Write("Enter another item or press enter to leave: ");
                string input1 = Console.ReadLine();
                stuff.Enqueue(input1);
                break;
            }​
            Console.Write("\nPress enter to return to the menu screen: "); {
            }
​}
        public static void Dequeue(Queue stuff)
        {
            Console.Write("Delete a thing yes or no: ");
            string input = Console.ReadLine();
​
            if (input.Contains("yes")) 
            {
                Console.Write("Delete another item or press enter to leave");
                input = Console.ReadLine();
                stuff.Dequeue();
            }
​
            Console.Write("\nPress enter to return to the menu screen: ");
        }​
        public static void Contains(Queue stuff)
        {
            Console.Write("Find an item: ");
            string SearchItem = Console.ReadLine();​
            Console.Write("{0}", stuff.Contains(SearchItem)); 
            Console.Write("\n\nPress enter to return to the menu screen: ");
        }​
        public static void ToArray(Queue stuff, object[] itemsarray) 
        {
            itemsarray = stuff.ToArray(); 
            foreach (object o in itemsarray)
            {
                Console.WriteLine(o + ""); 
            }
            Console.Write("\nPress enter to return to the menu screen: ");
        }
    }
}
